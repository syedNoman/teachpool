<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="LMV">
    <title>Index</title>
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">--}}

    {{--<link rel="stylesheet" href="{{asset('from/css/font-awesome.min.css')}}">--}}
    {{--<link href="{{asset('from/css/bootstrap.min.css')}}" rel="stylesheet">--}}


    {{--<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">--}}
    {{--<!-- End of head section HTML codes -->--}}


    {{--<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">--}}

    {{--<link rel="stylesheet" href="{{asset('css/style2.css')}}">--}}
    {{--<link href="{{asset('css/main.css')}}" rel="stylesheet">--}}
    <!-- End of head section HTML codes -->
    {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />--}}
    <link href="{{asset('from/css/main.css')}}" rel="stylesheet">
</head>
<body>
{{--@include('web.layout.header.nav')--}}

<div class="wrapper">
    <div id="content">

        @if(Session::has('successMsg'))
            <div id="alert-sucss" style="position: absolute;
    margin-left: 28%;
    width: 42%;
    margin-top: 26.5%;
    text-align: center;" class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">x</button>
                {{ Session::get('successMsg') }}</div>
        @endif

        <form  action="{{route('form-submit')}}" method="post"  enctype="multipart/form-data">
            @csrf
            <div class="row" style="border-bottom: 1px solid #000; padding-bottom: 10px; margin-bottom: 20px;">
                <div class="col-md-3">
                    <img src="images/logo.png" alt="logo">
                </div>
                <div class="col-md-6">
                    <div class="topheading">
                        <h1>TECHNOLOGY POOL FOUNDATION </h1>
                        <h2>Innovation Ambassador<br /> Programme</h2>
                        <h3 class="text-center">Application Form for Student & Professional</h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group uploadimg">
                        <div class="input-group">
						<span class="input-group-btn">
							<span id="place-text" class="btn btn-default btn-file">
								Place<br /> Your<br /> Photograph <input type="file" name="image" id="imgInp">
							</span>
						</span>
                        </div>
                        <img id="img-upload"/>
                        {{--<span id="imgInp2" class="btn btn-default btn-file">--}}
                        {{--Change <input type="file" id="imgInp">--}}
                        {{--</span>--}}
                    </div>
                </div>
            </div>
            <div class="notebox">
                <p>Please complete this form and return it to:</p>
                <p style="margin-bottom: 15px;">email: <a href="#.">ambassadors@technologypoolfoundation.org</a> </p>
                <h2>Notes</h2>
                <ul>
                    <li>Applications can take up to two/three days to process.</li>
                    <li>This form can be emailed; together with your CV, if available.</li>
                    <li>Correspondence with you will mainly be by email.  Please ensure that your email address is clearly legible.</li>
                    <li>If you wish to apply for “Innovation Skill Development Programme” then send us a covering letter mentioning you are interested to join this free certification course.</li>
                </ul>
                <p>Please TYPE all your details in this form where possible.</p>
            </div>
            <div class="form_1">
                <h1>Section 1 – Applicant details</h1>
                <h2>Personal details</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Title</label>
                            <div class="clearfix"></div>
                            <fieldset style="background-color: transparent;" id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}">
                                {{--<input id="title" type="text" class="form-control{{ $errors->has('other') ? ' is-invalid' : '' }}" name="other" value="{{ old('other') }}">--}}

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                                <span>
							Mr
							<input type="radio" name="title"  value="Mr" @if(old('title')=="Mr") checked @endif>
						</span>
                                <span>
							Mrs
                            <input type="radio" name="title"  value="Mrs" @if(old('title')=="Mrs") checked @endif >
						</span>
                                <span>
							Miss
                            <input type="radio" name="title" value="Miss" @if(old('title')=="Miss") checked @endif>
						</span>
                                <span>
							Ms
                            <input type="radio" name="title" value="Ms" @if(old('title')=="Ms") checked @endif >
						</span>
                                <span>
							Dr
                             <input type="radio" name="title" value="Dr" @if(old('title')=="Dr") checked @endif >
						</span>
                            </fieldset>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Other (please specify)</label>

                            <input id="other" type="text" class="form-control{{ $errors->has('other') ? ' is-invalid' : '' }}" name="other" value="{{ old('other') }}">

                            @if ($errors->has('other'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('other') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">First name</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}">

                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Middle name(s)</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input id="middle_name" type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" name="middle_name" value="{{ old('middle_name') }}">

                            @if ($errors->has('middle_name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Family name</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input id="family_name" type="text" class="form-control{{ $errors->has('family_name') ? ' is-invalid' : '' }}" name="family_name" value="{{ old('family_name') }}">

                            @if ($errors->has('family_name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('family_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Gender</label>
                            <fieldset style="background-color: transparent;" id="gender" type="text" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" value="{{ old('gender') }}">
                                <div class="clearfix"></div>
                                <span>
							Male
							<input type="radio" name="gender" value="Male" @if(old('gender')=="Male") checked @endif >
						</span>
                                <span>
							Female
							<input type="radio" name="gender" value="Female" @if(old('gender')=="Female") checked @endif>
						</span>

                            </fieldset>
                            @if ($errors->has('gender'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="">Date of birth</label>
                    </div>
                    <div class="col-md-12">

                        <div class="form-group">
                            {{--<input type="text" placeholder="DD" name="date_of_birth" class="form-control" >--}}
                            <input id="date_of_birth" type="date" class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" value="{{ old('date_of_birth') }}">

                            @if ($errors->has('date_of_birth'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_1">
                <h2>Current residential address</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Address</label>
                            {{--<textarea class="form-control"  rows="4"></textarea>--}}
                            <textarea id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" >{{ old('address') }}</textarea>

                            @if ($errors->has('address'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Postal city/town</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}">

                            @if ($errors->has('city'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Postcode</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input id="postcode" type="text" class="form-control{{ $errors->has('postcode') ? ' is-invalid' : '' }}" name="postcode" value="{{ old('postcode') }}">

                            @if ($errors->has('postcode'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">County</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input id="county" type="text" class="form-control{{ $errors->has('county') ? ' is-invalid' : '' }}" name="county" value="{{ old('county') }}">

                            @if ($errors->has('county'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('county') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">COUNTRY</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}">

                            @if ($errors->has('country'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Telephone number</label>
                            <input id="number" type="number" maxlength="9" minlength="8" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number') }}">

                            @if ($errors->has('number'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Country code</label>
                            <input id="country_code" type="text" class="form-control{{ $errors->has('country_code') ? ' is-invalid' : '' }}" name="country_code" value="{{ old('country_code') }}">

                            @if ($errors->has('country_code'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country_code') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Area/City code</label>
                            <input id="area_code" type="text" class="form-control{{ $errors->has('area_code') ? ' is-invalid' : '' }}" name="area_code" value="{{ old('area_code') }}">

                            @if ($errors->has('area_code'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('area_code') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Personal email address <br /> (please print very clearly)</label>
                            {{--<input type="text" placeholder="" class="form-control" >--}}
                            <input p  id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email')  }}">

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_1">
                <h2>Current employment address (if you are employed)</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{--<label for="">If you are currently unemployed please tick here</label>--}}
                            {{--<input type="radio">--}}
                            <fieldset style="background-color: transparent;" id="employed" type="text" class="form-control{{ $errors->has('employed') ? ' is-invalid' : '' }}" name="employed" value="{{ old('employed') }}">
                                <div class="clearfix"></div>
                                <span>
							If you are currently unemployed please tick here
							<input id="employ" type="checkbox" name="employed" value="1" >
                            </span>

                            </fieldset>
                            @if ($errors->has('employed'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('employed') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div id="employment-session">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Company name</label>
                                {{--<input type="text" placeholder="" class="form-control" >--}}
                                <input id="company_name" type="text"  class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{ old('company_name') }}">

                                @if ($errors->has('company_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Position/Job title</label>
                                {{--<input type="text" placeholder="" class="form-control" >--}}
                                <input id="job_title" type="text"  class="form-control{{ $errors->has('job_title') ? ' is-invalid' : '' }}" name="job_title" value="{{ old('job_title') }}">

                                @if ($errors->has('job_title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('job_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Department</label>
                                {{--<input type="text" placeholder="" class="form-control" >--}}
                                <input id="department" type="text"  class="form-control{{ $errors->has('department') ? ' is-invalid' : '' }}" name="department" value="{{ old('department') }}">

                                @if ($errors->has('department'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('department') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Company address</label>
                                {{--<textarea class="form-control"  rows="4">--}}
                                <textarea id="company_address" type="text"  class="form-control{{ $errors->has('company_address') ? ' is-invalid' : '' }}" name="company_address">{{ old('company_address') }}</textarea>

                                @if ($errors->has('company_address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form_1">
                    <h1>Section 2 – Education and qualifications</h1>
                    <h2>University education</h2>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Level</label>
                            </div>
                        </div>

                        <fieldset style="background-color: transparent;" id="education" type="text" class="form-control{{ $errors->has('education') ? ' is-invalid' : '' }}" name="education" value="{{ old('education') }}">
                            <div class="clearfix"></div>
                            <span>
							 Undergraduate
							<input type="radio" name="education" value="Undergraduate" @if(old('education')=="Undergraduate") checked @endif >
						</span>
                            <span>
							 Graduate/Master
							<input type="radio" name="education" value="Master" @if(old('education')=="Master") checked @endif>
						</span>


                            @if ($errors->has('education'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('education') }}</strong>
                                    </span>
                            @endif
                        </fieldset>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">University attended</label>
                                {{--<input type="text" placeholder="" class="form-control" >--}}
                                <input id="uni_name" type="text"  class="form-control{{ $errors->has('uni_name') ? ' is-invalid' : '' }}" name="uni_name" value="{{ old('uni_name') }}">

                                @if ($errors->has('uni_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('uni_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Subject studied</label>
                                {{--<input type="text" placeholder="" class="form-control" >--}}
                                <input id="subject_studies" type="text"  class="form-control{{ $errors->has('subject_studies') ? ' is-invalid' : '' }}" name="subject_studies" value="{{ old('subject_studies') }}">

                                @if ($errors->has('subject_studies'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('subject_studies') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Date of graduation</label>
                                {{--<input type="text" placeholder="" class="form-control" >--}}
                                <input id="date_of_graduation" type="date"  class="form-control{{ $errors->has('date_of_graduation') ? ' is-invalid' : '' }}" name="date_of_graduation" value="{{ old('date_of_graduation') }}">

                                @if ($errors->has('date_of_graduation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date_of_graduation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Level</label>
                            </div>
                        </div>
                        <fieldset style="background-color: transparent;" id="education_level" type="text" class="form-control{{ $errors->has('education_level') ? ' is-invalid' : '' }}" name="education_level" value="{{ old('education') }}">
                            <div class="clearfix"></div>
                            <div class="row col-md-12">
                    <span class="col-md-3">
                        <span class="form-group">
                            Postgraduate diploma
							<input type="radio" name="education_level" value="Postgraduate diploma" @if(old('education_level')=="Postgraduate diploma") checked @endif >
                        </span>
                    </span>
                                <span class="col-md-3">
                        <span class="form-group">
                            MS/MPhil
                            <input type="radio" name="education_level" value="MSMPhil" @if(old('education_level')=="MSMPhil") checked @endif >
                        </span>
                    </span>

                                <span class="col-md-3">
                        <span class="form-group">
                            Doctorate
                            <input type="radio" name="education_level" value="Doctorate" @if(old('education_level')=="Doctorate") checked @endif >
                        </span>
                    </span>
                            </div>
                        </fieldset>
                        @if ($errors->has('education_level'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('education_level') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">University attended</label>
                                <input id="diploma_university_name" type="text"  class="form-control{{ $errors->has('diploma_university_name') ? ' is-invalid' : '' }}" name="diploma_university_name" value="{{ old('diploma_university_name') }}">

                                @if ($errors->has('diploma_university_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('diploma_university_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Subject studied</label>
                                <input id="diploma_subject_studies" type="text"  class="form-control{{ $errors->has('diploma_subject_studies') ? ' is-invalid' : '' }}" name="diploma_subject_studies" value="{{ old('diploma_subject_studies') }}">

                                @if ($errors->has('diploma_subject_studies'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('diploma_subject_studies') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Date of graduation</label>
                                <input id="diploma_date_of_graduation" type="date"  class="form-control{{ $errors->has('diploma_date_of_graduation') ? ' is-invalid' : '' }}" name="diploma_date_of_graduation" value="{{ old('diploma_date_of_graduation') }}">

                                @if ($errors->has('diploma_date_of_graduation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('diploma_date_of_graduation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_1">
                    <h2>School education</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped table-light">
                            <thead>
                            <tr>
                                <th><strong>School attended (give full name and town)</strong></th>
                                <th><strong>O/A level (or High School equivalent): Subject name</strong></th>
                                <th><strong>Grade</strong></th>
                                <th><strong>Date achieved</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    {{--<input type="text" placeholder="School attended" class="form-control" >--}}
                                    <input id="school_name1" type="text"  class="form-control{{ $errors->has('school_name1') ? ' is-invalid' : '' }}" name="school_name1" value="{{ old('school_name1') }}">

                                    @if ($errors->has('school_name1'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_name1') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="O/A level" class="form-control" >--}}
                                    <input id="school_subject1" type="text"  class="form-control{{ $errors->has('school_subject1') ? ' is-invalid' : '' }}" name="school_subject1" value="{{ old('school_subject1') }}">

                                    @if ($errors->has('school_subject1'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_subject1') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Grade" class="form-control" >--}}
                                    <input id="school_grade1" type="text"  class="form-control{{ $errors->has('school_grade1') ? ' is-invalid' : '' }}" name="school_grade1" value="{{ old('school_grade1') }}">

                                    @if ($errors->has('school_grade1'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_grade1') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Date achieved" class="form-control" >--}}
                                    <input id="school_date_achieved1" type="date"  class="form-control{{ $errors->has('school_date_achieved1') ? ' is-invalid' : '' }}" name="school_date_achieved1" value="{{ old('school_date_achieved1') }}">

                                    @if ($errors->has('school_date_achieved1'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_date_achieved1') }}</strong>
                                    </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{--<input type="text" placeholder="School attended" class="form-control" >--}}
                                    <input id="school_name2" type="text"  class="form-control{{ $errors->has('school_name2') ? ' is-invalid' : '' }}" name="school_name2" value="{{ old('school_name2') }}">

                                    @if ($errors->has('school_name2'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_name2') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="O/A level" class="form-control" >--}}
                                    <input id="school_subject2" type="text"  class="form-control{{ $errors->has('school_subject2') ? ' is-invalid' : '' }}" name="school_subject2" value="{{ old('school_subject2') }}">

                                    @if ($errors->has('school_subject2'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_subject2') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Grade" class="form-control" >--}}
                                    <input id="school_grade2" type="text"  class="form-control{{ $errors->has('school_grade2') ? ' is-invalid' : '' }}" name="school_grade2" value="{{ old('school_grade2') }}">

                                    @if ($errors->has('school_grade2'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_grade2') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Date achieved" class="form-control" >--}}
                                    <input id="school_date_achieved2" type="text"  class="form-control{{ $errors->has('school_date_achieved2') ? ' is-invalid' : '' }}" name="school_date_achieved2" value="{{ old('school_date_achieved2') }}">

                                    @if ($errors->has('school_date_achieved2'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_date_achieved2') }}</strong>
                                    </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{--<input type="text" placeholder="School attended" class="form-control" >--}}
                                    <input id="school_name3" type="text"  class="form-control{{ $errors->has('school_name3') ? ' is-invalid' : '' }}" name="school_name3" value="{{ old('school_name3') }}">

                                    @if ($errors->has('school_name3'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_name3') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="O/A level" class="form-control" >--}}
                                    <input id="school_subject3" type="text"  class="form-control{{ $errors->has('school_subject3') ? ' is-invalid' : '' }}" name="school_subject3" value="{{ old('school_subject3') }}">

                                    @if ($errors->has('school_subject3'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_subject3') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Grade" class="form-control" >--}}
                                    <input id="school_grade3" type="text"  class="form-control{{ $errors->has('school_grade3') ? ' is-invalid' : '' }}" name="school_grade3" value="{{ old('school_grade3') }}">

                                    @if ($errors->has('school_grade3'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_grade3') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Date achieved" class="form-control" >--}}
                                    <input id="school_date_achieved3" type="text"  class="form-control{{ $errors->has('school_date_achieved3') ? ' is-invalid' : '' }}" name="school_date_achieved3" value="{{ old('school_date_achieved3') }}">

                                    @if ($errors->has('school_date_achieved3'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_date_achieved3') }}</strong>
                                    </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{--<input type="text" placeholder="School attended" class="form-control" >--}}
                                    <input id="school_name4" type="text"  class="form-control{{ $errors->has('school_name4') ? ' is-invalid' : '' }}" name="school_name4" value="{{ old('school_name4') }}">

                                    @if ($errors->has('school_name4'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_name4') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="O/A level" class="form-control" >--}}
                                    <input id="school_subject4" type="text"  class="form-control{{ $errors->has('school_subject4') ? ' is-invalid' : '' }}" name="school_subject4" value="{{ old('school_subject4') }}">

                                    @if ($errors->has('school_subject4'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_subject4') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Grade" class="form-control" >--}}
                                    <input id="school_grade4" type="text"  class="form-control{{ $errors->has('school_grade4') ? ' is-invalid' : '' }}" name="school_grade4" value="{{ old('school_grade4') }}">

                                    @if ($errors->has('school_grade4'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_grade4') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Date achieved" class="form-control" >--}}
                                    <input id="school_date_achieved4" type="text"  class="form-control{{ $errors->has('school_date_achieved4') ? ' is-invalid' : '' }}" name="school_date_achieved4" value="{{ old('school_date_achieved4') }}">

                                    @if ($errors->has('school_date_achieved4'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_date_achieved4') }}</strong>
                                    </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{--<input type="text" placeholder="School attended" class="form-control" >--}}
                                    <input id="school_name5" type="text"  class="form-control{{ $errors->has('school_name5') ? ' is-invalid' : '' }}" name="school_name5" value="{{ old('school_name5') }}">

                                    @if ($errors->has('school_name5'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_name5') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="O/A level" class="form-control" >--}}
                                    <input id="school_subject5" type="text"  class="form-control{{ $errors->has('school_subject5') ? ' is-invalid' : '' }}" name="school_subject5" value="{{ old('school_subject5') }}">

                                    @if ($errors->has('school_subject5'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_subject5') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Grade" class="form-control" >--}}
                                    <input id="school_grade5" type="text"  class="form-control{{ $errors->has('school_grade5') ? ' is-invalid' : '' }}" name="school_grade5" value="{{ old('school_grade5') }}">

                                    @if ($errors->has('school_grade5'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_grade5') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Date achieved" class="form-control" >--}}
                                    <input id="school_date_achieved5" type="text"  class="form-control{{ $errors->has('school_date_achieved5') ? ' is-invalid' : '' }}" name="school_date_achieved5" value="{{ old('school_date_achieved5') }}">

                                    @if ($errors->has('school_date_achieved5'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_date_achieved5') }}</strong>
                                    </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{--<input type="text" placeholder="School attended" class="form-control" >--}}
                                    <input id="school_name6" type="text"  class="form-control{{ $errors->has('school_name6') ? ' is-invalid' : '' }}" name="school_name6" value="{{ old('school_name6') }}">

                                    @if ($errors->has('school_name6'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_name6') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="O/A level" class="form-control" >--}}
                                    <input id="school_subject6" type="text"  class="form-control{{ $errors->has('school_subject6') ? ' is-invalid' : '' }}" name="school_subject6" value="{{ old('school_subject6') }}">

                                    @if ($errors->has('school_subject6'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_subject6') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Grade" class="form-control" >--}}
                                    <input id="school_grade6" type="text"  class="form-control{{ $errors->has('school_grade6') ? ' is-invalid' : '' }}" name="school_grade6" value="{{ old('school_grade6') }}">

                                    @if ($errors->has('school_grade6'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_grade6') }}</strong>
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {{--<input type="text" placeholder="Date achieved" class="form-control" >--}}
                                    <input id="school_date_achieved6" type="text"  class="form-control{{ $errors->has('school_date_achieved6') ? ' is-invalid' : '' }}" name="school_date_achieved6" value="{{ old('school_date_achieved6') }}">

                                    @if ($errors->has('school_date_achieved6'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('school_date_achieved6') }}</strong>
                                    </span>
                                    @endif
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <h2 class="alert-light" style="padding: 10px;">Other professional qualifications</h2>
                    <p>Please give details of any other professional qualifications that you have gained</p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Qualification gained</label>
                                <input id="other_university_name" type="text"  class="form-control{{ $errors->has('other_university_name') ? ' is-invalid' : '' }}" name="other_university_name" value="{{ old('other_university_name') }}">

                                @if ($errors->has('other_university_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('other_university_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Date of graduation</label>
                                <input id="date_of_othergraduation" type="date"  class="form-control{{ $errors->has('date_of_othergraduation') ? ' is-invalid' : '' }}" name="date_of_othergraduation" value="{{ old('date_of_othergraduation') }}">

                                @if ($errors->has('date_of_othergraduation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date_of_othergraduation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Professional association</label>
                                <input id="professional_association" type="text"  class="form-control{{ $errors->has('professional_association') ? ' is-invalid' : '' }}" name="professional_association" value="{{ old('professional_association') }}">

                                @if ($errors->has('professional_association'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('professional_association') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="">Qualification gained</label>--}}
                    {{--<input type="text" placeholder="" class="form-control" >--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="">Date of graduation</label>--}}
                    {{--<input type="text" placeholder="" class="form-control" >--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="form_1">
                    <h1>Section 3 – Referees section </h1>
                    <h2>First referee</h2>
                    <p>I have known the applicant and to the best of my knowledge and belief, consider him/her to be a fit and proper person to be a member of Technology Pool Foundation.</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Name (BLOCK CAPITALS)</label>
                                <input id="reference_name" type="text"  class="form-control{{ $errors->has('reference_name') ? ' is-invalid' : '' }}" name="reference_name" value="{{ old('reference_name') }}">

                                @if ($errors->has('reference_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('reference_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Occupation</label>
                                <input id="reference_occupation" type="text"  class="form-control{{ $errors->has('reference_occupation') ? ' is-invalid' : '' }}" name="reference_occupation" value="{{ old('reference_occupation') }}">

                                @if ($errors->has('reference_occupation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('reference_occupation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Address</label>
                                {{--<textarea class="form-control"  rows="4"></textarea>--}}
                                <textarea rows="4" id="reference_address" type="reference_address"  class="form-control{{ $errors->has('reference_address') ? ' is-invalid' : '' }}" name="reference_address">{{ old('reference_address') }}</textarea>

                                @if ($errors->has('reference_address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('reference_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Email address</label>
                                <input id="reference_email" type="email"  class="form-control{{ $errors->has('reference_email') ? ' is-invalid' : '' }}" name="reference_email" value="{{ old('reference_email') }}">

                                @if ($errors->has('reference_email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('reference_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_1">
                    <h1>Section 4 – The applicant’s signature & endorsement</h1>
                    <h2>This section must be signed/endorsed by applicant.</h2>
                    <p>This is an electronic form, and you can endorse an electronic signature while typing it.  </p>
                    <ul>
                        <li>I apply to the Technology Pool Foundation (TPF) “Innovation Ambassador Programme” to brush up my innovation skills and employ the same in my daily life, academic learning, and professional workplace. </li>
                        <li>I confirm that I am a fit and bona fide person to join this programme and promote this cause to make Pakistan’s globally competitive and make my homeland an emerging knowledge economy in the world.</li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Name (BLOCK CAPITALS)</label>
                                <input id="name_capital" type="text"  class="form-control{{ $errors->has('name_capital') ? ' is-invalid' : '' }}" name="name_capital" value="{{ old('name_capital') }}">

                                @if ($errors->has('name_capital'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name_capital') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                        {{--<label for="">Signature</label>--}}
                        {{--<input type="text" placeholder="" class="form-control" >--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Date</label>
                                <input id="date" type="date"  class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" value="{{ old('date') }}">

                                @if ($errors->has('date'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="{{asset('from/js/popper.min.js')}}"></script>
<script src="{{asset('from/js/bootstrap.min.js')}}"></script>
<script src="{{asset('from/js/aos.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/aos.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(".alert-success").fadeTo(2000, 500).slideUp(1000, function(){
        $(".alert-success").alert('close');
    });
</script>
<script>
    $( "#employ" ).ready(function () {
        $('#employment-session').hide();

        $( "#employ" ).change(function() {
            check = $("#employ").is(":checked");
            if(check) {
                $('#employment-session').slideDown('3000');
            } else {
//            alert("Checkbox is unchecked.");
                $('#employment-session').slideUp('3000');
            }

        });
    });
</script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
<script>
    /*$(document).ready(function() {
     var stickyNavTop = $('.stickymenu').offset().top;
     var stickyNav = function(){
     var scrollTop = $(window).scrollTop();
     if (scrollTop > stickyNavTop) {
     $('.stickymenu').addClass('sticky');
     } else {
     $('.stickymenu').removeClass('sticky');
     }
     };
     stickyNav();
     $(window).scroll(function() {
     stickyNav();
     });
     });	*/

    //    position: absolute;
    //    margin-left: -108px;
    //    margin-top: 95px;

    $(document).ready( function() {
        $(document).on('change', '.uploadimg .btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
            // $('#place-text').text('changeee');
        });

        $('.uploadimg .btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.uploadimg .input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });

        $("#imgInp2").change(function(){
            readURL(this);
        });
    });
</script>
</body>
</html>
