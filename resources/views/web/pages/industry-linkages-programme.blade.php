@extends('web.layout.app')
@section('content')
    <div id="content">
        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div><img src="images/ab.png" alt="" style="width: 100%;"></div>
        <div class="innercontant">
            <h1>Our Programmes</h1>
            <h3>Industry Linkages Programme</h3>
            <br>
            <p>Pakistan is a developing country and there is dire need that university and industry must work together so that resources are synergized for innovation and industrial competitiveness in the country. This will lead the Pakistan as innovation hub resulting new products and process benefiting the industries and economy for regional and national development. Likewise, such linkages can also benefit to the society to present innovative ideas which make industries environment friendly for the local communities. </p>
            <br>
            <p>  TPF encourages the students and scholars to get maximum exposure of Pakistani industries related to their study and research areas. This will help them to enhance their study and career profile and make them successful in terms of getting better financial rewards after the graduation from university.</p>
            <br>
            <p>  Below is the list of services, TFP can arrange for you and/or for your university:</p>
            <br>
            <ul>
                <li> Free Industrial Trips of Chosen Industry</li>
                <li> Internship Placement</li>
                <li> Dissertation & Thesis: Industrial Data Collection</li>
                <li>  University-Industry Joint Research Projects <br > In addition to this, if you any question regarding to Industry linkage<br /> Programme, please send us query:</li>
            </ul>
            <br>
            <p> Our Programme Team will contact you shortly, once receive the query.</p>
        </div>
    </div>
@endsection


