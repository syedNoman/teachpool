@extends('web.layout.app')
@section('content')
    <div id="content">
        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div><img src="images/ab.png" alt="" style="width: 100%;"></div>
        <div class="innercontant">
            <h1>Our Mission</h1>
            <br>
            <p>Our mission is to inspire and empower people to change their world.</p>
            <br>
            <p>From leaders who are already making an impact, to people who are interested in becoming more involved, but don’t know where to start, our goal is to make our programs accessible to anyone, anywhere. We’ll equip civic innovators, young leaders, and everyday citizens with the skills and tools they need to create change in their communities.  </p>
            <br>
            <p> It’s a big job, and we’re just getting started. Learn about our first set of projects and join us in this experiment in citizenship for the 21st century.</p>
            <br>
            <h2>Objectives</h2>
            <br>
            <p> The stated purposes of technology pool foundation are:</p>
            <ul>
                <li>To organize, charter and supervise service clusters to cater the industries development needs.</li>
                <li>To coordinate the activities and standardize the administration of knowledge and technology clubs for industries.</li>
                <li> To create and foster a spirit of technology understanding among the industries to achieve global competitiveness.</li>
                <li>To promote the principles of outcome-based research and development resulting toward technology development to benefit the industry and society.</li>
                <li>To take an active part in arrangement of design thinking workshops to provide innovative solutions to economy and society.</li>
                <li>To unite the innovations clubs in the bonds of friendship, good fellowship and mutual understanding.</li>
                <li>To provide a forum for the open discussion of all matters of creativity, invention, and innovation resulting in technology development for the society.</li>
                <li>To encourage innovative-minded students to serve their school, college, and university and promote technology development related business ideas serving the industries and society.</li>
            </ul>
            <br>
        </div>
    </div>
@endsection


