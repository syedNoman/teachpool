@extends('web.layout.app')
@section('content')
    <div id="content">
        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div><img src="images/ab.png" alt="" style="width: 100%;"></div>
        <div class="innercontant">
            <h1>Innovation Ambassador Programme</h1>
            <blockquote>
                JOIN US TO IGNITE THE DISCUSSION ON PAKISTAN INDUSTRIES AS “WELL-PLANNED INDUSTRIES” & MOVING TOWARDS AS PER QUAID-E-AZAM’s VISION
            </blockquote>
            <h1>Become the Voice</h1>
            <br>
            <p>At TPF, we are making our efforts to advocate the pressing need to have a research-intensive, high-technology, knowledge-based economy as a significant agenda of national development. But we cannot do it alone; we would like you to become our ambassadors and lead the discussion on the agenda of Pakistan as a Knowledge Economy and Technology Pool Foundation as a cornerstone to this notion of economic development. Knowledge Ambassadors Programme is an effective liaison between the students and Technology Pool Foundation.</p>
            <br>
            <p> Students will be selected as Technology Ambassadors against a certain evaluation criteria for each cohort and will be given the opportunity to work with the outreach and communication team of TPF in order to raise awareness, drive advocacy campaigns on social media and to spur the discussions among their networks. On completion of the first cohort, Technology Ambassadors will be awarded with certificates of participation. The participants will also be contesting for Technology Ambassador Award.</p>
            <br>
            <h1>Role of the Ambassadors:</h1>
            <br>
            <p>Spark discussions on the topics relevant to ‘knowledge economy’ based on research & development, and science & technology.
                Raising awareness about Technology Pool Foundation as a substantial step towards transforming Pakistan into knowledge based economy.
                Generating content to plug into social media campaigns of Technology Pool Foundation.</p>
            <br>
            <p>Documenting and sharing stories from the students highlighting their needs and challenges through their journey of education.
                Promoting TPF’s Innovation Ambassadors Programme among the students for the next cohort.</p>
            <br>
            <h1>Application:</h1>
            <p>You will hear back from us in about 2 to 3 days with the results of your application. If you are accepted you will be invited to our day long working session in Lahore and will be briefed on what the next steps are. If you believe in and have the zeal to initiate and lead a meaningful discussion on the subjects of innovation, entrepreneurship, industry-academia linkage, research and country’s resultant economic acceleration, Technology Ambassador Programme is certainly a forum for you.</p>
            <br>
            <div class="text-center">
                <a href="#." class="btn btn-dark">Apply Now</a>
            </div>
            <br>
            <p>    US Economy & Role of Technology Transfer: Innovation & Entrepreneurship
                What is Innovation: Benefits for Society and Economy
                Intellectual Property Rights & 21st Century Knowledge Economies

                In addition to this, if you any question regarding to Innovation Ambassador Programme, please send us query:</p>
            <p> Our Programme Team will contact you shortly, once receive the query.</p>
        </div>
    </div>
@endsection


