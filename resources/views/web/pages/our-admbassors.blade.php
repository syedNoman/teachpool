@extends('web.layout.app')
@section('content')
    <div id="content">
        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div><img src="images/ab.png" alt="" style="width: 100%;"></div>
        <div class="innercontant">
            <h1>Our Ambassadors</h1>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover table-striped">
                    <tr>
                        <td><strong>Ms. Minaal Ali</strong></td>
                        <td>Head of Lahore Region</td>
                    </tr>
                    <tr>
                        <td><strong>Ms. Natasha Murtaza</strong></td>
                        <td>Head of Faisalabad Region</td>
                    </tr>
                    <tr>
                        <td><strong>Ms. Fatima Muneer</strong></td>
                        <td>Campus Director, Lahore College for Women University</td>
                    </tr>
                    <tr>
                        <td><strong>Ms. Aqsa Amir</strong></td>
                        <td>Campus Director, Kinnaird College for Women</td>
                    </tr>
                    <tr>
                        <td><strong>Mr. Ali Husnain</strong></td>
                        <td>Campus Director, National College of Business Administration & Economics</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection


