@extends('web.layout.app')
@section('content')
    <div id="content">
        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div><img src="images/ab.png" alt="" style="width: 100%;"></div>
        <div class="innercontant">
            <h1>Our Programmes</h1>
            <h3>Career Development Programme</h3>
            <br>
            <p>In 21st century, the traditional industries are being transformed through innovations and technology advancements. So, the nature of workplace and emerging career in knowledge-based economy would be different from today career path. </p>
            <br>
            <p> It is important for you to understand your own personality traits and related field of study, and afterward career options; which are quite tricky and complicated. </p>
            <br>
            <p> At TPF, you have the opportunity to attend and learn the free lectures/speeches of industry leaders, policy makers, educationists, and career counselors; and see what is going on in future. Overall, this learning will help you to explore the opportunities available out of classroom environment.</p>
            <br>
            <p>  Below is the proposed list of events, TFP can arrange at your university campus:</p>
            <br>
            <ul>
                <li> Career Development Planning</li>
                <li> Understand Yourself and Your Desired Course</li>
                <li> CV Writing and Interview Techniques</li>
                <li>    Industry Exposure & Networking for Career Development
                    <br >
                    In addition to this, if you any question regarding to Career Development Programme, please send us query:</li>
            </ul>
            <br>
            <p> Our Programme Team will contact you shortly, once receive the query.</p>
        </div>
    </div>
@endsection


