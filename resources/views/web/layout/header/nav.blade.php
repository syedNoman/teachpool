
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3><img src="images/logotpff.png" alt="" /></h3>
        </div>

        <ul class="list-unstyled components">
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <!--<i class="fas fa-home"></i>-->
                    Your Voice
                </a>
                <!-- <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Home 1</a>
                    </li>
                    <li>
                        <a href="#">Home 2</a>
                    </li>
                    <li>
                        <a href="#">Home 3</a>
                    </li>
                </ul> -->
            </li>
            <li>
                <a href="{{route('mission')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Vision & Mission
                </a>
                {{--<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">--}}
                    <a href="{{route('board-of-advisors')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Board of Advisors
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="#">Page 1</a>
                    </li>
                    <li>
                        <a href="#">Page 2</a>
                    </li>
                    <li>
                        <a href="#">Page 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{route('our-ambassador')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Our Ambassadors
                </a>
            </li>
            <li>
                <a href="{{route('innovation-ambassador-programme')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Innovation Ambassador Programme
                </a>
            </li>
            <li>
                <a href="{{route('career-development-programme')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Career Development Programme
                </a>
            </li>
            <li>
                <a href="{{route('industry-linkages-programme')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Industry Linkages Programme
                </a>
            </li>
            <li>
                <a href="{{route('scholarship-outreach-programme')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Scholarship Outreach Programme
                </a>
            </li>
            <li>
                <a href="{{route('contact-us')}}">
                    <!--<i class="fas fa-home"></i>-->
                    Contact Us
                </a>
            </li>
        </ul>

        <ul class="list-unstyled fonticon">
            <li>
                <a href="#."><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href="#."><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href="#."><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </li>
        </ul>
    </nav>
