<!DOCTYPE html>
<!--[if lte IE 8 ]>
<html lang="en" class="no-js oldie">
<![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="no-js ie9">
<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<link href="style.css" rel="stylesheet" type="text/css">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7527714/7615372/css/fonts.css" />
    <link href="https://fonts.googleapis.com/css?family=Maitree:400,500,600" rel="stylesheet">
    <link rel="stylesheet" href="style.css?0308a" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <title>Contact Us - Technology Pool Foundation</title>
    <script type='text/javascript' src='js/jquery.js?ver=2.1.14'></script>
</head>
<body class="home page-template page-template-template-home page-template-template-home-php page page-id-120 has-dashicons subbrand-default " data-template="base.twig">

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVXZ7JC"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<header class="site-header js-site-header">
    <nav class="nav-primary--desktop js-nav-primary" role="navigation">
        <div class="nav-primary__header-logo js-nav-primary__nav-toggle"> <a class="nav-primary__site-logo" href="http://technologypoolfoundation.org"> Technology Pool Foundation</a> </div>
        <div class="nav-primary__drawer js-nav-primary__drawer">
            <div class="nav-primary__home-header">
                <h1 class="nav-primary__indicator">Menu</h1>
            </div>
            <ul class="nav-primary__menu">
                <li class="nav-primary__home"> <a class="nav-primary__home-logo" href="http://technologypoolfoundation.org">Home</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="your-voice.html">Your Voice</a>

                </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="vision-mission.html">Vision & Mission</a> </li>

                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="board-experts.html">Board of Advisors</a> </li>

                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="our-ambassadors.html">Our Ambassadors</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="innovation-ambassador-programe.html">Innovation Ambassador Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="Career-Development.html">Career Development Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="Industry-Linkages.html">Industry Linkages Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="Scholarship-Outreach.html">Scholarship Outreach Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="contact-us.html">Contact Us</a> </li>
            </ul>
            <div class="nav-primary__utility">
                <p class="nav-primary__social-follows">
                    <a class="nav-primary__social-follow icon-twitter-icon" href="#" title="Follow on Twitter" aria-label="Follow on Twitter" target="_blank"></a>
                    <a class="nav-primary__social-follow icon-fb-icon" href="#" title="Follow on Facebook" aria-label="Follow on Facebook" target="_blank"></a>
                    <a class="nav-primary__social-follow icon-instagram-icon js-instagram-icon" href="#" title="Follow on Instagram" aria-label="Follow on Instagram" target="_blank"></a> </p>

            </div>
        </div>
    </nav>
    <nav class="nav-primary--mobile" role="navigation">
        <div class="nav-primary__header-logo js-nav-primary__nav-toggle">
            <div class="nav-primary__toggle"> <span></span> <span></span> <span></span> <span></span> </div>
            <a class="nav-primary__site-logo" href="http://technologypoolfoundation.org">Technology Pool Foundation</a> </div>
        <div class="nav-primary__drawer js-nav-primary__drawer">
            <div class="nav-primary__home-header">
                <h1 class="nav-primary__indicator">Menu</h1>
            </div>
            <ul class="nav-primary__menu">
                <li class="nav-primary__home"> <a class="nav-primary__home-logo" href="http://technologypoolfoundation.org">Home</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="your-voice.html">Your Voice</a> </li>
                <li class="nav-primary__item--top-level current_page_item"> <a class="nav-primary__link" href="vision-mission.html">Vision & Mission</a> </li>

                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="board-experts.html">Board of Advisors</a> </li>

                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="our-ambassadors.html">Our Ambassadors</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="innovation-ambassador-programe.html">Innovation Ambassador Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="Career-Development.html">Career Development Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="Industry-Linkages.html">Industry Linkages Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="Scholarship-Outreach.html">Scholarship Outreach Programme</a> </li>
                <li class="nav-primary__item--top-level"> <a class="nav-primary__link" href="contact-us.html">Contact Us</a> </li>
            </ul>
            <div class="nav-primary__utility">
                <p class="nav-primary__social-follows">
                    <a class="nav-primary__social-follow icon-twitter-icon" href="#" title="Follow on Twitter" aria-label="Follow on Twitter" target="_blank"></a>
                    <a class="nav-primary__social-follow icon-fb-icon" href="#" title="Follow on Facebook" aria-label="Follow on Facebook" target="_blank"></a>
                    <a class="nav-primary__social-follow icon-instagram-icon js-instagram-icon" href="#" title="Follow on Instagram" aria-label="Follow on Instagram" target="_blank"></a> </p>

            </div>
        </div>
    </nav>

</header>
<main class="main-content " role="main">
    <!--<section class="homepage  ">
    <div class="homepage__panel homepage__final-state" style="background-image: url(images/bg.jpg);">
    </div>
    <div class="homepage__panel homepage__initial-state">
    <div class="homepage__content">
    </div>
    </div>
    <div class="homepage__panel">
    <div class="homepage__content">

    <div class="homepage__logo--wrapper">
    <div class="homepage__logo homepage__logo--video"></div>
    <div class="homepage__logo homepage__logo--white"></div>
    </div>

    <h1 class="homepage__headline">Pakistan’s Future <br />
      Economic Life is based <br />
     on  Well-Planned Industries.</h1>
    <div class="homepage__action">
    <p class="homepage__attribution"> &ndash; Our Vision</p>
    </div>
    <div class="homepage__action">
    <a class="homepage__button" href="your-voice.php">Add Your Voice</a>
    </div>
    </div>
    </div>
    </section>-->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="images/bg.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/bg.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/bg.jpg" alt="First slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <section id="carwrepp">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="images/bg.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<footer class="site-footer" role="contentinfo">
    <div class="site-footer__wrapper">
        <div class="site-footer__contents">
        </div>
    </div>
</footer>
<div class="back-to-top js-back-to-top">
    <p class="back-to-top__label">Top</p>
</div>
<script type='text/javascript' src='https://www.obama.org/wp-content/themes/obamafoundation/assets/js/source.js?ver=0.0.4'></script>
<script type='text/javascript' src='https://www.obama.org/wp-includes/js/wp-embed.min.js?ver=4.9.2'></script>
<script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58e692e7c2341621"></script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyDsnqr-BgEQKKvG_-aayb5FO7ILgLhIkBo&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"0207d3137c","applicationID":"30404694","transactionName":"YlRbNUpWD0QHVxBYCVseeAJMXg5ZSUABXBZZUE0EFV8OWgM=","queueTime":0,"applicationTime":278,"atts":"ThNYQwJMHEo=","errorBeacon":"bam.nr-data.net","agent":""}</script>
<script src="js/bootstrap.min.js" ></script>
<script src="js/jquery.min.js" ></script>
</body>
</html>
