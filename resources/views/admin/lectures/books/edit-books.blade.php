<form id="editfrom" method="POST" action="/lectures/{{ $lecture->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ method_field('put') }}
        {{ csrf_field() }}

        <div class="form-group">
            <label for="exampleInputName">Books Name</label>
            <input class="form-control" id="bookid" value="{{ $lecture->name }}" name="name"
                   type="text" placeholder="Enter book Name"
                   required>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
