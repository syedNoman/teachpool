@extends('admin.layouts.app')
@section('content')
    @include("admin.layouts.lift-manu-bar")
    <div class="main-content">
        <div class="main-content-inner">
            @include('admin.layouts.navbar')
            <div class="page-content">
                <div class="page-header">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <h1>
                                Tables
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Lectures Video Table
                                </small>
                            </h1>
                        </div>
                        <div class="col-sm-2">
                            <form action="/lectures?role=2" method="GET">
                                <input class="form-control" id="nameid" name="role" value="2"
                                       type="hidden"
                                       required>
                                <div class="input-group">
                                    <input type="text" style=" width:200px; height: 33px;" class="form-control"
                                           name="searchTerm" placeholder="Search for..."
                                           value="{{ isset($_GET['searchTerm']) ? $_GET['searchTerm'] : '' }}">
                                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit" style="height: 33px;border: none;">Search</button>
                                    </span>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.page-header -->
                <br/>
                <div class="card-header">
                    <button class="btn-info btn-sm pull-left b" data-toggle="modal" data-target="#exampleModal">Add
                        lectures Video
                    </button>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Add
                                                            lectures Video
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <form id="form" method="POST" action="/lectures?role=2"
                                                          enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            {{ csrf_field() }}
                                                            <input class="form-control" id="nameid" name="role"
                                                                   value="2"
                                                                   type="hidden"
                                                                   required>
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    Name
                                                                </lable>
                                                                <input class="form-control" id="nameid" name="name"
                                                                       type="text" placeholder="Enter Name"
                                                                       required>
                                                            </div>
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    Scholar Name
                                                                </lable>
                                                                <select id="exampleInputCat" class="form-control"
                                                                        name="scholar_id" value="">
                                                                    @foreach($scholars as $key => $scholar)

                                                                        <option value="{{ $scholar->id }}">{{ $scholar->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    Link
                                                                </lable>
                                                                <input class="form-control" id="myUrl" name="link"
                                                                       type="text" placeholder="Enter Name"
                                                                       required>
                                                            </div>
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    video description
                                                                </lable>
                                                                <textarea rows="4" class="form-control" id="myUrl" name="desc"
                                                                       type="text" placeholder="Enter video description"
                                                                          ></textarea>
                                                            </div>
                                                            {{--<div>--}}
                                                            {{--<a href="javascript:void(0)" id="myBtn">Go</a>--}}
                                                            {{--</div>--}}
                                                            {{--<div>YouTube ID: <span id="myId"></span>--}}

                                                            {{--</div>--}}
                                                            {{--<div>Embed code: <pre id="myCode"></pre></div>--}}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">NO#</th>
                                        <th>Name</th>
                                        <th>Desc</th>
                                        <th>Links</th>
                                        <th class="col-md-1">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lectures as $key => $lecture)
                                        <tr>
                                            <td>
                                                {{ ($lectures->currentpage() - 1) * $lectures->perpage() + 1 + $key }}

                                                {{--{{ ($admins->perpage() + 1 + $key }}--}}

                                            </td>
                                            <td>{{ $lecture->name }}</td>
                                            <td>
                                            @if(isset($lecture->desc))
                                                {{ $lecture->desc }}
                                            @else
                                                {{' No Desc '}}
                                            @endif
                                            </td>
                                            {{--<td>{{ $lecture->desc }}</td>--}}
                                            {{--<td><a href="{{ $lecture->link }}" target="_blank">{{ $lecture->link }}</a></td>--}}
                                            <td>{!! preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"200\" height=\"100\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$lecture->link ) !!}
                                            <td>

                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                            data-toggle="modal"
                                                            data-action="/lectures/{{ $lecture->id }}/edit"
                                                            data-target="#exampleModal2">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </button>
                                                    <button title="Delete" type="button" data-toggle="modal"
                                                            data-target="#delete-modal"
                                                            data-action="/lectures/{{ $lecture->id }}"
                                                            class="show-delete-modal btn btn-xs btn-danger">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                <div class="text-right">{{ $lectures->links() }}</div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit table
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        $(document).ready(function () {
            $('#play-video').on('click', function (ev) {

                $("#video")[0].src += "&autoplay=1";
                ev.preventDefault();

            });
        });


        // Youtube URL to EMbed


        $(document).ready(function () {
            var myId;

            $('#myId').click(function () {
                var myUrl = $('#myId').val();
                myId = getId(myUrl);

                $('#myId').html(myId);

                $('#myCode').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen></iframe>');
            });
        })


        function getId(url) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = url.match(regExp);

            if (match && match[2].length == 11) {
                return match[2];
            } else {
                return 'error';
            }
        }

    </script>

@endsection

