<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar')
        } catch (e) {
        }
    </script>
    <ul class="nav nav-list">
        <li class="active">
            <a href="/">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Home </span>
            </a>
            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="/users">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text">Users</span>
            </a>
            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-tag"></i>
                <span class="menu-text">Pages</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li class="">
                    <a href="/lectures?role=1">
                        <i class="menu-icon fa fa-list"></i>

                        <span class="menu-text">page1</span>
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/lectures?role=2">
                        <i class="menu-icon fa fa-list"></i>
                        <span class="menu-text">page2</span>
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/lectures?role=3">
                        <i class="menu-icon fa fa-list"></i>

                        <span class="menu-text">page3</span>
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        {{--<li class="">--}}
            {{--<a href="#" class="dropdown-toggle">--}}
                {{--<i class="menu-icon fa fa-tag"></i>--}}
                {{--<span class="menu-text">Cources</span>--}}

                {{--<b class="arrow fa fa-angle-down"></b>--}}
            {{--</a>--}}
            {{--<b class="arrow"></b>--}}
            {{--<ul class="submenu">--}}
                {{--<li class="">--}}
                    {{--<a href="/cources?role=1">--}}
                        {{--<i class="menu-icon fa fa-list"></i>--}}

                        {{--<span class="menu-text">PDF</span>--}}
                    {{--</a>--}}
                    {{--<b class="arrow"></b>--}}
                {{--</li>--}}
                {{--<li class="">--}}
                    {{--<a href="/cources?role=2">--}}
                        {{--<i class="menu-icon fa fa-list"></i>--}}
                        {{--<span class="menu-text">Videos</span>--}}
                    {{--</a>--}}
                    {{--<b class="arrow"></b>--}}
                {{--</li>--}}
                {{--<li class="">--}}
                    {{--<a href="/cources?role=3">--}}
                        {{--<i class="menu-icon fa fa-list"></i>--}}

                        {{--<span class="menu-text">Books</span>--}}
                    {{--</a>--}}
                    {{--<b class="arrow"></b>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="">--}}
            {{--<a href="/events">--}}
                {{--<i class="menu-icon fa fa-list"></i>--}}

                {{--<span class="menu-text">Events</span>--}}
            {{--</a>--}}

            {{--<b class="arrow"></b>--}}
        {{--</li>--}}

        {{--<li class="">--}}
            {{--<a href="/coerce">--}}
                {{--<i class="menu-icon fa fa-list"></i>--}}

                {{--<span class="menu-text">cources</span>--}}
            {{--</a>--}}

            {{--<b class="arrow"></b>--}}
        {{--</li>--}}
    </ul>
    </li>
    </ul>
    <div style="margin-top: 2210%; !important;" class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
           data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>
