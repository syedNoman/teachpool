<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta nadadadcme="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Od') }}</title>

<!-- Styles -->
<!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="{{  asset('assets/css/bootstrap.min.css') }}"/>
<link rel="stylesheet" href="{{  asset('assets/css/bootstrap-timepicker.min.css') }}"/>
{{--<link rel="stylesheet" href="{{  asset('assets/css/bootstrap-datetimepicker.min.css') }}"/>--}}
<link rel="stylesheet" href="{{  asset('assets/font-awesome/4.5.0/css/font-awesome.min.css') }}"/>

<!-- page specific plugin styles -->

<!-- text fonts -->
<link rel="stylesheet" href="{{  asset('assets/css/fonts.googleapis.com.css') }}"/>

<!-- ace styles -->
<link rel="stylesheet" href="{{  asset('assets/css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style"/>

<!--[if lte IE 9]>
<link rel="stylesheet" href="{{  asset('assets/css/ace-part2.min.css') }}" class="ace-main-stylesheet"/>
<![endif]-->
<link rel="stylesheet" href="{{  asset('assets/css/ace-skins.min.css') }}"/>
<link rel="stylesheet" href="{{  asset('assets/css/ace-rtl.min.css') }}"/>

<!--[if lte IE 9]>
<link rel="stylesheet" href="{{  asset('assets/css/ace-ie.min.css') }}"/>
<![endif]-->
<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/summernote/summernote.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/css/dropzone.min.css') }}"/>

<!-- inline styles related to this page -->
<style>
    .b {
        margin-bottom: 10px;

    }

    .page-header {
        border: none;
    }
</style>
<!-- ace settings handler -->
<script src="{{  asset('assets/js/ace-extra.min.js') }}"></script>

<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

<!--[if lte IE 8]>
<script src="{{  asset('assets/js/html5shiv.min.js') }}"></script>
<script src="{{  asset('assets/js/respond.min.js') }}"></script>
<![endif]-->

@yield('style')

