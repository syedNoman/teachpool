<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('admin.layouts.header-script')
</head>
<body class="no-skin">
<div class="container-fluid">
<!-- Header Area Start -->
@include('admin.layouts.header')
<!-- Header Area End -->
<!-- Banner Area Start -->

@yield('content')

<!-- Footer Area Start -->
@include('admin.layouts.form-footer')
<!-- Footer Area End -->


<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
<!-- Wrapper End -->


<!-- basic scripts -->

<!--[if !IE]> -->
@include('admin.layouts.footer-script')
@yield('script')
</div>

</body>
</html>
