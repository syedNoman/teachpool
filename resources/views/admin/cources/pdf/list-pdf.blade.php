@extends('admin.layouts.app')
@section('content')
    @include("admin.layouts.lift-manu-bar")
    <div class="main-content">
        <div class="main-content-inner">
            @include('admin.layouts.navbar')
            <div class="page-content">
                <div class="page-header">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <h1>
                                Tables
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Cource PDF Table
                                </small>
                            </h1>
                        </div>
                        <div class="col-sm-2">
                            <form action="/cources?role=1" method="GET">
                                <input class="form-control" id="nameid" name="role" value="1"
                                       type="hidden"
                                       required>
                                <div class="input-group">
                                    <input type="text" style=" width:200px; height: 33px;" class="form-control"
                                           name="searchTerm" placeholder="Search for..."
                                           value="{{ isset($_GET['searchTerm']) ? $_GET['searchTerm'] : '' }}">
                                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit" style="height: 33px;border: none;">Search</button>
                                    </span>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.page-header -->
                <br/>
                <div class="card-header">
                    <button class="btn-info btn-sm pull-left b" data-toggle="modal" data-target="#exampleModal">Add
                        cources PDF
                    </button>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Add
                                                            cources
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <form id="form" method="POST" action="/cources?role=1"
                                                          enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            {{ csrf_field() }}
                                                            <input class="form-control" id="nameid" name="role"
                                                                   value="1"
                                                                   type="hidden"
                                                                   required>
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    Name
                                                                </lable>
                                                                <input class="form-control" id="nameid" name="name"
                                                                       type="text" placeholder="Enter Name"
                                                                       required>
                                                            </div>
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    Scholar Name
                                                                </lable>
                                                                <select id="exampleInputCat" class="form-control"
                                                                        name="scholar_id" value="">
                                                                    @foreach($scholars as $key => $scholar)

                                                                        <option value="{{ $scholar->id }}">{{ $scholar->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                    About
                                                                </lable>
                                                                <textarea name="detail"
                                                                          id="summernote">{{ old('detail') }}</textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Add PDF File</label>
                                                                <input class="form-control" id="id-input-file-2"
                                                                       name="pdf_path"
                                                                       type="file">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">NO#</th>
                                        <th>Name</th>
                                        <th>Detail</th>
                                        <th>PDF View/Download</th>
                                        <th class="col-md-1">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cources as $key => $lecture)
                                        <tr>
                                            <td>
                                                {{ ($cources->currentpage() - 1) * $cources->perpage() + 1 + $key }}
                                            </td>
                                            <td>{{ $lecture->name }}</td>
                                            <td>{!! $lecture->detail !!}</td>
                                            <td><a title="view" class="btn btn-xs btn-primary"
                                                   href="{{ asset('uploads/pdf/'.$lecture->pdf) }}">
                                                    <i class="ace-icon fa fa-eye bigger-120"></i>
                                                </a>
                                            </td>

                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                            data-toggle="modal"
                                                            data-action="/cources/{{ $lecture->id }}/edit"
                                                            data-target="#exampleModal2">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </button>
                                                    <button title="Delete" type="button" data-toggle="modal"
                                                            data-target="#delete-modal"
                                                            data-action="/cources/{{ $lecture->id }}"
                                                            class="show-delete-modal btn btn-xs btn-danger">
                                                         <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                <div class="text-right">{{ $cources->links() }}</div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit table
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });
        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection

