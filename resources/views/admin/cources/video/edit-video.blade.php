<form id="editfrom" method="POST" action="/cources/{{ $lecture->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ method_field('put') }}
        {{ csrf_field() }}

        <div class="form-group">
            <label for="exampleInputName">video Name</label>
            <input class="form-control" id="nameid" value="{{ $lecture->name }}" name="name"
                   type="text" placeholder="Enter video Name"
                   required>
        </div>
        <div class="form-group">
            <label for="exampleInputName">Link</label>
            <input class="form-control" id="linkid" value="{{ $lecture->link }}" name="link"
                   type="text" placeholder="Enter link"
                   required>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
