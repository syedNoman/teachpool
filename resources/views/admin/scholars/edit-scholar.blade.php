<form id="editfrom" method="POST" action="/scholar/{{ $scholar->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ method_field('put') }}
        {{ csrf_field() }}

        <div class="form-group">
            <label for="exampleInputName">Scholar Name</label>
            <input class="form-control" id="nameid" value="{{ $scholar->name }}" name="name"
                   type="text" placeholder="Enter Name"
                   required>
        </div>
        <div class="form-group">
            <lable for="exampleInputCat" class="control-label">About</lable>
            <textarea class="form-control" name="about" id="summernote">{{ $scholar->about }}</textarea>
        </div>
        <div class="form-group">
            @if ($scholar->picture)
                <img src="{{ asset('uploads/scholars/'.$scholar->picture) }}" width="100px">
            @else
                <p>No image found</p>
            @endif
            <input type="file" name="picture_path"/>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
