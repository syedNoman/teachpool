<form id="editfrom" method="POST" action="/events/{{ $event->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ method_field('put') }}
        {{ csrf_field() }}

        <div class="form-group">
            <lable for="exampleInputCat" class="control-label">Event Name</lable>
            <input class="form-control" id="nameid" value="{{ $event->event_name }}" name="event_name"
                   type="text" placeholder="Enter Name"
                   required>
        </div>
        <div class="form-group">
            <label for="id-date-picker-1">Date Picker</label>

            <div class="input-group">
                <input class="form-control date-picker"
                       id="id-date-picker-1"
                       type="text" name="date_of_event" value="{{ $event->date_of_event }}" data-date-format="dd-mm-yyyy"/>
                <span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>
            </div>
        </div>



        <div class="form-group">
            <lable for="exampleInputCat" class="control-label">Interested People</lable>
            <input class="form-control" id="nameid" value="{!!$event->description  !!}" name="description"
                   type="text"
                   required>
        </div>

        <div class="form-group">
            @if ($event->gallery_1)
                <img src="{{ asset('uploads/events/'.$event->gallery_1) }}" width="100px">
            @else
                <p>No image found</p>
            @endif
            <input type="file" name="gallery_path_1"/>
        </div>
        <div class="form-group">
            @if ($event->gallery_2)
                <img src="{{ asset('uploads/events/'.$event->gallery_2) }}" width="100px">
            @else
                <p>No image found</p>
            @endif
            <input type="file" name="gallery_path_2"/>
        </div>
        <div class="form-group">
            @if ($event->gallery_3)
                <img src="{{ asset('uploads/events/'.$event->gallery_3) }}" width="100px">
            @else
                <p>No image found</p>
            @endif
            <input type="file" name="gallery_path_3"/>
        </div>
        <div class="form-group">
            @if ($event->gallery_4)
                <img src="{{ asset('uploads/events/'.$event->gallery_4) }}" width="100px">
            @else
                <p>No image found</p>
            @endif
            <input type="file" name="gallery_path_4"/>
        </div>
        <div class="form-group">
            @if ($event->gallery_5)
                <img src="{{ asset('uploads/events/'.$event->gallery_5) }}" width="100px">
            @else
                <p>No image found</p>
            @endif
            <input type="file" name="gallery_path_5"/>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
