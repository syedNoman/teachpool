@extends('admin.layouts.app')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/date-time-picker/bootstrap-datetimepicker.css')}}">
    @endsection
@section('content')
    @include("admin.layouts.lift-manu-bar")
    <div class="main-content">
        <div class="main-content-inner">
            @include('admin.layouts.navbar')
            <div class="page-content">
                <div class="page-header">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <h1>
                                Tables
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                   Join Event Peoples
                                </small>
                            </h1>
                        </div>
                        <div class="col-sm-2">
                            <form action="/events" method="GET">
                                <div class="input-group">
                                    <input type="text" style=" width:200px; height: 33px;" class="form-control"
                                           name="searchTerm" placeholder="Search for..."
                                           value="{{ isset($_GET['searchTerm']) ? $_GET['searchTerm'] : '' }}">
                                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="submit" style="height: 33px;border: none;">Search</button>
                                    </span>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.page-header -->
                <br/>
                <div class="card-header">
                    <button class="btn-info btn-sm pull-left b" data-toggle="modal" data-target="#exampleModal">Add
                       Events
                    </button>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Add
                                                            Event
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <form method="POST" action="/events"
                                                          enctype="multipart/form-data" class="" id="form">
                                                        <div class="modal-body">
                                                            {{ csrf_field() }}

                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">Event Name</lable>
                                                                <input class="form-control" id="nameid" name="event_name"
                                                                       type="text" placeholder="Enter Name"
                                                                required>
                                                            </div>

                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">Discretion People</lable>
                                                                <textarea name="description"
                                                                          id="summernote">{{ old('about') }}</textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">Event Total Seat</lable>
                                                                <input class="form-control" id="nameid" name="total_seats"
                                                                       type="number" placeholder="Enter Name"
                                                                       required>
                                                            </div>

                                                                <div class="form-group">
                                                                    <div class='input-group date' id='datetimepicker1'>

                                                                        <input placeholder="Enter Event Date" type='text' class="form-control" name="date_of_event" />
                                                                        <span class="input-group-addon">
                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                            <div class="form-group">
                                                                <div class='input-group date' id='datetimepickertime'>
                                                                    <input placeholder="Enter Event time" type='text' class="form-control" name="time_of_event" />
                                                                    <span class="input-group-addon">
                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                        </span>
                                                                </div>
                                                            </div>


                                                            {{--<div class="form-group">--}}
                                                                {{--<lable for="exampleInputCat" class="control-label">Interested People</lable>--}}
                                                                {{--<input class="form-control" id="nameid" name="interested_people"--}}
                                                                       {{--type="text"--}}
                                                                       {{--required>--}}
                                                            {{--</div>--}}


                                                            {{--<div class="form-group">--}}
                                                                {{--<label for="exampleInputEmail1">Event Picture one</label>--}}
                                                                {{--<input class="form-control" id="id-input-file-2"--}}
                                                                       {{--name="picture_path_1"--}}
                                                                       {{--type="file">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label for="exampleInputEmail1">Event Picture Two</label>--}}
                                                                {{--<input class="form-control" id="id-input-file-2"--}}
                                                                       {{--name="picture_path_2"--}}
                                                                       {{--type="file">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label for="exampleInputEmail1">Event Picture Three</label>--}}
                                                                {{--<input class="form-control" id="id-input-file-2"--}}
                                                                       {{--name="picture_path_3"--}}
                                                                       {{--type="file">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label for="exampleInputEmail1">Event Picture Four</label>--}}
                                                                {{--<input class="form-control" id="id-input-file-2"--}}
                                                                       {{--name="picture_path_4"--}}
                                                                       {{--type="file">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<label for="exampleInputEmail1">Event Picture Five</label>--}}
                                                                {{--<input class="form-control" id="id-input-file-2"--}}
                                                                       {{--name="picture_path_5"--}}
                                                                       {{--type="file">--}}
                                                            {{--</div>--}}

                                                            <div class="widget-body">
                                                                <div class="widget-main">
                                                                    <div class="form-group">
                                                                        <div class="col-xs-12">
                                                                            <input multiple="" name="picture_path_5[]" type="file" id="id-input-file-3" />
                                                                        </div>
                                                                    </div>

                                                                    <label>
                                                                        <input type="checkbox" name="file-format" id="id-file-format" class="ace" />
                                                                        <span class="lbl"> Allow only images</span>
                                                                    </label>
                                                                </div>
                                                            </div>




                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">NO#</th>
                                        <th>Name</th>
                                        <th>Phone #</th>
                                        {{--<th>Event Name</th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key => $user)

                                            <tr>
                                            <td>
                                                {{ $user->$key+1}}

                                            </td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->phone_no }}</td>
                                            {{--<td>{{ $user->users->event_id}}</td>--}}

                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                {{--<div class="text-right">{{ $users->links() }}</div>--}}
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit table
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('assets/date-time-picker/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/date-time-picker/moment-with-locales.js') }}"></script>
    <script>
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });



//        $('#timepicker1').timepicker({
//            minuteStep: 1,
//            showSeconds: true,
//            showMeridian: false,
//            disableFocus: true,
//            icons: {
//                up: 'fa fa-chevron-up',
//                down: 'fa fa-chevron-down'
//            }
//        }).on('focus', function() {
//            $('#timepicker1').timepicker('showWidget');
//        }).next().on(ace.click_event, function(){
//            $(this).prev().focus();
//        });


// drop zone


        // Summernote
        $('#summernote').summernote({
            height: 200
        });

    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'D-M-Y'
            });

        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepickertime').datetimepicker({
                format: 'LT'
            });
        });
    </script>
@endsection

