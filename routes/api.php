<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 Route::get('/get-scholars', 'api\ApiController@get_scholars');
 Route::get('/get-lectures', 'api\ApiController@get_lectures');
 Route::get('/get-cources', 'api\ApiController@get_cources');
 Route::get('/get-books', 'api\ApiController@get_books');
 Route::get('/get-cource-books', 'api\ApiController@get_cource_books');
     Route::get('/get-lectures-videos', 'api\ApiController@get_videos');
     Route::get('/get-cource-videos', 'api\ApiController@get_cource_videos');
Route::get('/get-pdf', 'api\ApiController@get_pdf');
Route::get('/get-cource-pdf', 'api\ApiController@get_cource_pdf');
Route::get('/add-view', 'api\ApiController@add_views');
 Route::get('/add-cource-view', 'api\ApiController@add_cource_views');
 Route::get('/add-download-pdf', 'api\ApiController@add_downloads_pdf');
 Route::get('/add-download-pdf', 'api\ApiController@add_downloads_pdf');
 Route::get('/add-download-books', 'api\ApiController@add_downloads_books');
 Route::get('/add-download-books', 'api\ApiController@add_downloads_books');
Route::get('/get-events', 'api\ApiController@get_events');
 Route::get('/get-upcoming-events', 'api\ApiController@get_up_events');
 Route::get('/get-old-events', 'api\ApiController@get_old_events');
 Route::post('/joind-event', 'api\ApiController@add_joind_events');
// Route::get('/joind-event', 'api\ApiController@joind_events');
 Route::get('/get-name', 'api\ApiController@get_names');
 Route::get('/old-event', 'api\ApiController@old_event');
 Route::get('/new-event', 'api\ApiController@new_event');
// Route::get('/new-event', 'api\ApiController@new_event');




//Route::get('listen', function(){
//    return view('listenbroadcast');
//});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



//http://192.168.15.134:8000/api/get-scholars
//http://192.168.15.134:8000/api/get-lectures
//http://127.0.0.1:8000/api/get-lectures?role=3&scholar_id=4
//http://127.0.0.1:8000/api/get-cources?role=3&scholar_id=4
//http://127.0.0.1:8000/api/get-videos
//http://127.0.0.1:8000/api/get-cource-videos
//http://192.168.15.134:8000/api/add-view?role=2&lecture_id=11
//http://127.0.0.1:8000/api/get-pdf
//http://127.0.0.1:8000/api/get-cource-pdf
//http://192.168.15.134:8000/api/add-download-pdf?lecture_id=9
//http://127.0.0.1:8000/api/get-books
//http://127.0.0.1:8000/api/get-cource-books
//http://192.168.15.134:8000/api/add-download-books?lecture_id=21
//http://192.168.15.134:8000/api/joind-event?event_id=2
//http://192.168.15.134:8000/api/get-events
//http://192.168.15.134:8000/api/get-upcoming-events
//http://192.168.15.134:8000/api/get-old-events