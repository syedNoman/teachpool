<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/login', 'HomeController@index')->name('home');
Route::get('/', 'WebController@index');
Route::get('/mission', 'WebController@mission')->name('mission');
Route::get('/board-of-advisors', 'WebController@boa')->name('board-of-advisors');
Route::get('/our-ambassador', 'WebController@admbassors')->name('our-ambassador');
Route::get('/innovation-ambassador-programme', 'WebController@iap')->name('innovation-ambassador-programme');
Route::get('/career-development-programme', 'WebController@cdp')->name('career-development-programme');
Route::get('/industry-linkages-programme', 'WebController@ilp')->name('industry-linkages-programme');
Route::get('/scholarship-outreach-programme', 'WebController@sop')->name('scholarship-outreach-programme');
Route::get('/contact-us', 'WebController@contact')->name('contact-us');

Route::post('/from-submit', 'WebController@FromSubmit')->name('from-submit');

Route::get('/join-people/{id}', 'admin\EventController@joinpeople')->name('join-people');

Route::get('/logout', 'HomeController@logout')->name('logout');
Auth::routes();
Route::resource('/scholar', 'admin\ScholarController');
Route::resource('/lectures', 'admin\LectureController');
Route::resource('/events', 'admin\EventController');
Route::resource('/cources', 'admin\CourceController');



Route::get('event', function(){
    event(new \App\Events\TaskEvent('New Event Announce'));
});

Route::get('listen', function(){
    return view('listenbroadcast');
});

Route::get('/send', 'WebController@send')->name('send');

//Route::get('/lectures', 'admin\LectureController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'HomeController@getuser')->name('users');
Route::get('/view-form/{id}', 'HomeController@viewForm')->name('view-form');



Route::get('/form-get', 'HomeController@formGet')->name('users');
Route::post('/search', 'HomeController@search')->name('search');
Route::get('/pdf', 'HomeController@pdf')->name('pdf');
