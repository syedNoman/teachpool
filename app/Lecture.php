<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
   protected $table = 'lectures';

   protected $fillable = [

       'scholar_id',
       'name',
       'detail',
       'pdf',
       'download',
       'view',
       'link',
       'desc',
       'role',

   ];
}
