<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'password',
        'title',
        'picture',
        'other',
        'middle_name',
        'family_name',
        'gender',
        'date_of_birth',
        'address',
        'city',
        'postcode',
        'county',
        'country',
        'country_code',
        'area_code',
        'number',
        ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function events()
    {
        return $this->belongsToMany(Event::class,'user_event','user_id','event_id');
    }
}
