<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cource extends Model
{
    protected $table = 'cources';

    protected $fillable = [

        'scholar_id',
        'name',
        'detail',
        'pdf',
        'download',
        'view',
        'link',
        'desc',
        'role',

    ];
}
