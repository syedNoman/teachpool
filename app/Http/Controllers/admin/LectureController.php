<?php

namespace App\Http\Controllers\admin;

use App\Lecture;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LectureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Lecture::where('role', '=', $request->role)->paginate(20);
        if ($request->has('searchTerm')) {
            $searchTerm = $request->get('searchTerm');
            $lectures = Lecture::where('name', 'like', '%' . $searchTerm . '%')->where('role', '=', $request->role)->paginate(20);
        } else {
            $lectures = Lecture::where('role', '=', $request->role)->paginate(20);
        }
        $scholars = User::where('role', '=', 2)->get();
        if ($request->role == 1) {
            return view('admin.lectures.pdf.list-pdf', compact('lectures', 'scholars'));
        } elseif ($request->role == 2) {
            return view('admin.lectures.video.list-video', compact('lectures', 'scholars'));

        } elseif ($request->role == 3) {
            return view('admin.lectures.books.list-books', compact('lectures', 'scholars'));

        } else {
            return 'Page Not Found';
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('pdf_path')) {
            $file = $request->file('pdf_path');
            $destinationPath = public_path() . '/uploads/pdf';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);


            $request->merge(['pdf' => $filename]);
        }
//        $request->request->add(['role'=>1]);
        $create = Lecture::create($request->all());

        if ($create) {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lecture = Lecture::findOrFail($id);
        if ($lecture->role == 1) {
            return view('admin.lectures.pdf.edit-pdf', compact('lecture'));
        } elseif ($lecture->role == 2) {
            return view('admin.lectures.video.edit-video', compact('lecture'));

        } elseif ($lecture->role == 3) {
            return view('admin.lectures.books.edit-books', compact('lecture'));

        } else {
            return 'Page Not Nound';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $pdf = Lecture::find($id);

        if ($file = $request->file('pdf_path')) {
            $destinationPath = public_path() . '/uploads/pdf';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);

            $request->merge(['pdf' => $filename]);
            @unlink(public_path('uploads/pdf/' . $pdf->pdf));
        }

        $pdf->update($request->all());

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Lecture::findOrFail($id);
        $product->delete();

        return back();
    }
}
