<?php

namespace App\Http\Controllers\admin;

use App\Event;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->has('searchTerm')) {
            $searchTerm = $request->get('searchTerm');
            $events = Event::where('event_name', 'like', '%' . $searchTerm . '%')->paginate(20);
        } else {
            $events = Event::latest()->paginate(20);
        }
        if ($request->is('events')) {
            return view('admin.events.list-event', compact('events'));

        } else if ($request->is('gallery')) {

            return view('admin.gallery.list-event', compact('events'));
        } else {
            return 'page not found';
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->time_of_event);
        $count = 1;
        foreach ($request->picture_path_5 as $file) {
//        if ($request->file('picture_path_5')) {
//            $file = $request->file('picture_path_1');
            $destinationPath = public_path() . '/uploads/events';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);


            $request->merge(['gallery_' . $count => $filename]);
            $count++;
        }
//        if ($request->file('picture_path_2')) {
//            $file = $request->file('picture_path_2');
//            $destinationPath = public_path() . '/uploads/events';
//            $filename = $file->getClientOriginalName();
//            $filename = time() . $filename;
//            $file->move($destinationPath, $filename);
//
//
//            $request->merge(['gallery_2' => $filename]);
//        }
//        if ($request->file('picture_path_3')) {
//            $file = $request->file('picture_path_3');
//            $destinationPath = public_path() . '/uploads/events';
//            $filename = $file->getClientOriginalName();
//            $filename = time() . $filename;
//            $file->move($destinationPath, $filename);
//
//
//            $request->merge(['gallery_3' => $filename]);
//        }
//        if ($request->file('picture_path_4')) {
//            $file = $request->file('picture_path_4');
//            $destinationPath = public_path() . '/uploads/events';
//            $filename = $file->getClientOriginalName();
//            $filename = time() . $filename;
//            $file->move($destinationPath, $filename);
//
//
//            $request->merge(['gallery_4' => $filename]);
//        }
//        if ($request->file('picture_path_5')) {
//            $file = $request->file('picture_path_5');
//            $destinationPath = public_path() . '/uploads/events';
//            $filename = $file->getClientOriginalName();
//            $filename = time() . $filename;
//            $file->move($destinationPath, $filename);
//
//
//            $request->merge(['gallery_5' => $filename]);
//        }

        $request->request->add([
            'seats_left'=>$request->total_seats,
        ]);
        $create = Event::create($request->all());

//        $event = new Event();
//        $event->users()->attach($request->users);

        if ($create) {
            event(new \App\Events\TaskEvent('A new Event Establish'));
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        return view('admin/events/edit-event', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $scho = Event::find($id);

        if ($file = $request->file('picture_path')) {
            $destinationPath = public_path() . '/uploads/events';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);

            $request->merge(['picture' => $filename]);
            @unlink(public_path('uploads/events/' . $scho->picture));
        }

        $scho->update($request->all());

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Event::findOrFail($id);
        $product->delete();

        return back();
    }

    public function joinpeople($id){
//        dd('ok');
        $resuts=Event::where('id',$id)->first();
        $users=$resuts->users;
       if($users) {
           return view('admin.events.joinuser-event', compact('users'));
       }

    }
}
