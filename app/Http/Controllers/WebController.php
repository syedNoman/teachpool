<?php

namespace App\Http\Controllers;

use App\Mail\UserRegistr;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Mail;

class WebController extends Controller
{

    public function send()
    {

        $title = 'title';
        $content = 'content';

        Mail::to('nomannafees043@gmail.com')->send(new UserRegistr('','this is body'));
//        Mail::send('mail.reminder', ['title' => $title, 'content' => $content], function ($message)
//        {
//
//            $message->from('nomannafees043@gmail.com', 'Eva');
//
//            $message->to('nomannafees043@gmail.com');
//
//        });


        return response()->json(['message' => 'Request completed']);
    }
    public function index(){
        return view('web.pages.index');
    }
    public function mission(){
        return view('web.pages.vision_mission');
    }
    public function boa(){
        return view('web.pages.boa');
    }
    public function admbassors(){
        return view('web.pages.our-admbassors');
    }
    public function iap(){
        return view('web.pages.innovation-ambassador-programme');
    }
    public function cdp(){
        return view('web.pages.career-development-programme');
    }
    public function ilp(){
        return view('web.pages.industry-linkages-programme');
    }
    public function sop(){
        return view('web.pages.scholarship-outreach-programme');
    }
    public function contact(){
        return view('web.pages.contact-us');
    }


    public function FromSubmit(Request $request)
    {
//        dd($request->all());

        $this->validate($request,[

            'title'=>'required',
            'other'=>'required',
            'first_name'=>'required',
            'middle_name'=>'required',
            'family_name'=>'required',
            'gender'=>'required',
            'date_of_birth'=>'required',
            'address'=>'required',
            'city'=>'required',
            'postcode'=>'required',
            'county'=>'required',
            'country'=>'required',
            'country_code'=>'required',
            'area_code'=>'required',
            'number'=>'required',
            'email' => 'required|string|email|max:255|unique:users',

            'education'=>'required',
            'uni_name'=>'required',
            'subject_studies'=>'required',
            'date_of_graduation'=>'required',
            'education_level'=>'required',
            'diploma_university_name'=>'required',
            'diploma_subject_studies'=>'required',
            'diploma_date_of_graduation'=>'required',

            'school_name1'=>'required',
            'school_subject1'=>'required',
            'school_grade1'=>'required',
            'school_date_achieved1'=>'required',
//
//            'school_name2'=>'required',
//            'school_subject2'=>'required',
//            'school_grade2'=>'required',
//            'school_date_achieved2'=>'required',
//
//            'school_name3'=>'required',
//            'school_subject3'=>'required',
//            'school_grade3'=>'required',
//            'school_date_achieved3'=>'required',
//
//            'school_name4'=>'required',
//            'school_subject4'=>'required',
//            'school_grade4'=>'required',
//            'school_date_achieved4'=>'required',
//
//            'school_name5'=>'required',
//            'school_subject5'=>'required',
//            'school_grade5'=>'required',
//            'school_date_achieved5'=>'required',
//
//            'school_name6'=>'required',
//            'school_subject6'=>'required',
//            'school_grade6'=>'required',
//            'school_date_achieved6'=>'required',
//
//            'other_university_name'=>'required',
//            'qualification_gained'=>'required',
//            'date_of_othergraduation'=>'required',
//            'professional_association'=>'required',

//            'reference_name'=>'required',
//            'reference_occupation'=>'required',
//            'reference_address'=>'required',
//            'reference_email'=>'required',
//            'name_capital'=>'required',
//            'date'=>'required',

        ]);

        if ($request->file('image')) {
            $file = $request->file('image');
            $destinationPath = 'assets/uploads/user';
            $filename = $file->getClientOriginalName();
            $filename = time() . $filename;
            $file->move($destinationPath, $filename);
            $request->merge(['picture' => $filename]);
        }

        $user = User::create($request->all());

        if($request->employed =='1'){
            $this->validate($request,[
                'company_name'=>'required',
                'job_title'=>'required',
                'department'=>'required',
                'company_address'=>'required',
            ]);

            DB::table('employment_detail')->insert([

                'user_id' => $user->id,
                'employed' => $request->employed,
                'company_name' => $request->company_name,
                'job_title' => $request->job_title,
                'department' => $request->department,
                'company_address' => $request->company_address,
            ]);
        }

        DB::table('education')->insert([
            'user_id' => $user->id,
            'education' => $request->education,
            'uni_name' => $request->uni_name,
            'subject_studies' => $request->subject_studies,
            'date_of_graduation' => $request->date_of_graduation,
            'education_level' => $request->education_level,
            'diploma_university_name' => $request->diploma_university_name,
            'diploma_subject_studies' => $request->diploma_subject_studies,
            'diploma_date_of_graduation' => $request->diploma_date_of_graduation,

            'school_name1' => $request->school_name1,
            'school_subject1' => $request->school_subject1,
            'school_grade1' => $request->school_grade1,
            'school_date_achieved1' => $request->school_date_achieved1,

            'school_name2' => $request->school_name2,
            'school_subject2' => $request->school_subject2,
            'school_grade2' => $request->school_grade2,
            'school_date_achieved2' => $request->school_date_achieved2,

            'school_name3' => $request->school_name3,
            'school_subject3' => $request->school_subject3,
            'school_grade3' => $request->school_grade3,
            'school_date_achieved3' => $request->school_date_achieved3,

            'school_name4' => $request->school_name4,
            'school_subject4' => $request->school_subject4,
            'school_grade4' => $request->school_grade4,
            'school_date_achieved4' => $request->school_date_achieved4,

            'school_name5' => $request->school_name5,
            'school_subject5' => $request->school_subject5,
            'school_grade5' => $request->school_grade5,
            'school_date_achieved5' => $request->school_date_achieved5,

            'school_name6' => $request->school_name6,
            'school_subject6' => $request->school_subject6,
            'school_grade6' => $request->school_grade6,
            'school_date_achieved6' => $request->school_date_achieved6,
        ]);

        DB::table('other_professional_qualifications')->insert([
            'user_id' => $user->id,
            'other_university_name'=>$request->other_university_name,
//            'qualification_gained'=>$request->qualification_gained,
            'date_of_othergraduation'=>$request->date_of_othergraduation,
            'professional_association'=>$request->professional_association,
        ]);

        DB::table('referees')->insert([
            'user_id' => $user->id,
            'reference_name'=>$request->reference_name,
            'reference_occupation'=>$request->reference_occupation,
            'reference_address'=>$request->reference_address,
            'reference_email'=>$request->reference_email,
            'name_capital'=>$request->name_capital,
            'date'=>$request->date,
        ]);


        if($user){
            $snd_userMail=$request->email;
            Mail::to($snd_userMail)->send(new UserRegistr('','this is body'));
            Mail::to('adnanch204904@gmail.com')->send(new UserRegistr('','this is body'));
            $request->session()->flash('successMsg','Saved succesfully!');
            return redirect('/contact-us');
//            return('save');
        }

    }


}
