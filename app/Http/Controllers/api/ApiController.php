<?php

namespace App\Http\Controllers\api;

use App\Event;
use App\Lecture;
use App\Cource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{

    //this api work get all scholars in user table with parameter role=2
    public function get_scholars(Request $request)
    {
        $scholars = User::where('role', '=', 2)->get(['id', 'name', 'picture', 'about']);
        if ($scholars->count() > 0) {
            $response['status'] = true;

            $response['data'] = $scholars;
            foreach ($scholars as $scholar) {
                $scholar['picture'] = url('/uploads/scholars/' . $scholar->picture);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Scholars not found';
        }
        return response()->json($response);
    }

    //this api work get all lecture (exp:pdf,video,books) with parameter role and scholar id

    public function get_lectures(Request $request)
    {
        if ($request->role == 1) {
            $lectures = Lecture::where('role', '=', 1)->where('scholar_id', $request->scholar_id)->get(['name', 'pdf']);
            if ($lectures->count() > 0) {
                $response['status'] = true;

                $response['data'] = $lectures;
                foreach ($lectures as $lecture) {
                    $lecture['pdf'] = url('/uploads/pdf/' . $lecture->pdf);
                }

            } else {
                $response['status'] = false;
                $response['message'] = 'Lecture not found';
            }
            return response()->json($response);
        } else if ($request->role == 2) {
            $lectures = Lecture::where('role', '=', 2)->where('scholar_id', $request->scholar_id)->get(['name', 'link', 'view', 'download', 'created_at']);
            if ($lectures->count() > 0) {
                $response['status'] = true;

                $response['data'] = $lectures;
                foreach ($lectures as $lecture) {
                    $lecture['year'] = $lecture->created_at->format('Y');
                    unset($lecture['created_at']);
                    parse_str(parse_url($lecture->link, PHP_URL_QUERY), $my_array_of_vars);
                    $lecture['code'] = $my_array_of_vars['v'];

                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Lecture not found';
            }
            return response()->json($response);
        } else if ($request->role == 3) {
            $lectures = Lecture::where('role', '=', 3)->where('scholar_id', $request->scholar_id)->get(['name', 'pdf']);
            if ($lectures->count() > 0) {
                $response['status'] = true;

                $response['data'] = $lectures;

            } else {
                $response['status'] = false;

                $response['message'] = 'Lecture Not Found';
            }
            return response()->json($response);
        } else {

            $response['status'] = false;

            $response['message'] = 'No Data Found';
        }
        return response()->json($response);
    }
    public function get_cources(Request $request)
    {
        if ($request->role == 1) {
            $cources = Cource::where('role', '=', 1)->where('scholar_id', $request->scholar_id)->get(['name', 'pdf']);
            if ($cources->count() > 0) {
                $response['status'] = true;

                $response['data'] = $cources;
                foreach ($cources as $lecture) {
                    $lecture['pdf'] = url('/uploads/cource/pdf/' . $lecture->pdf);
                }

            } else {
                $response['status'] = false;
                $response['message'] = 'Lecture not found';
            }
            return response()->json($response);
        } else if ($request->role == 2) {
            $cources = Cource::where('role', '=', 2)->where('scholar_id', $request->scholar_id)->get(['name', 'link', 'view', 'download', 'created_at']);
            if ($cources->count() > 0) {
                $response['status'] = true;

                $response['data'] = $cources;
                foreach ($cources as $lecture) {
                    $lecture['year'] = $lecture->created_at->format('Y');
                    unset($lecture['created_at']);
                    parse_str(parse_url($lecture->link, PHP_URL_QUERY), $my_array_of_vars);
                    $lecture['code'] = $my_array_of_vars['v'];

                }
            } else {
                $response['status'] = false;
                $response['message'] = 'Lecture not found';
            }
            return response()->json($response);
        } else if ($request->role == 3) {
            $lectures = Cource::where('role', '=', 3)->where('scholar_id', $request->scholar_id)->get(['name', 'pdf']);
            if ($lectures->count() > 0) {
                $response['status'] = true;

                $response['data'] = $lectures;

            } else {
                $response['status'] = false;

                $response['message'] = 'Lecture Not Found';
            }
            return response()->json($response);
        } else {

            $response['status'] = false;

            $response['message'] = 'No Data Found';
        }
        return response()->json($response);
    }

    //this api work get all books lecture with parameter role

    public function get_books(Request $request)
    {
        $lectures = Lecture::where('role', '=', 3)->get(['id', 'name', 'pdf']);
        if ($lectures->count() > 0) {
            $response['status'] = true;

            $response['data'] = $lectures;
            foreach ($lectures as $lecture) {
                $lecture['pdf'] = url('/uploads/pdf/' . $lecture->pdf);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'books not found';
        }
        return response()->json($response);
    }
    public function get_cource_books(Request $request)
    {
        $lectures = Cource::where('role', '=', 3)->get(['id', 'name', 'pdf']);
        if ($lectures->count() > 0) {
            $response['status'] = true;

            $response['data'] = $lectures;
            foreach ($lectures as $lecture) {
                $lecture['pdf'] = url('/uploads/cource/pdf/' . $lecture->pdf);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Cource Books Not Found';
        }
        return response()->json($response);
    }

    //this api work get all video lecture with parameter role

    public function get_videos(Request $request)
    {
        $lectures = Lecture::where('role', '=', 2)->get(['id', 'name', 'link','desc', 'view', 'created_at']);
        if ($lectures->count() > 0) {
            $response['status'] = true;
            $response['data'] = $lectures;

            foreach ($lectures as $lecture) {
                $lecture['year'] = $lecture->created_at->format('Y');
                unset($lecture['created_at']);
                parse_str(parse_url($lecture->link, PHP_URL_QUERY), $my_array_of_vars);
                $lecture['code'] = $my_array_of_vars['v'];

            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Video Not Found';
        }
        return response()->json($response);
    }
    public function get_cource_videos(Request $request)
    {
        $lectures = Cource::where('role', '=', 2)->get(['id', 'name', 'link','desc', 'view', 'created_at']);
        if ($lectures->count() > 0) {
            $response['status'] = true;
            $response['data'] = $lectures;

            foreach ($lectures as $lecture) {
                $lecture['year'] = $lecture->created_at->format('Y');
                unset($lecture['created_at']);
                parse_str(parse_url($lecture->link, PHP_URL_QUERY), $my_array_of_vars);
                $lecture['code'] = $my_array_of_vars['v'];

            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Cource Video Not Found';
        }
        return response()->json($response);
    }

    //this api work get all pdf lecture with parameter role

    public function get_pdf(Request $request)
    {
        $lectures = Lecture::where('role', '=', 1)->get(['id', 'name', 'detail', 'pdf']);
        if ($lectures->count() > 0) {
            $response['status'] = true;

            $response['data'] = $lectures;
            foreach ($lectures as $lecture) {
                $lecture['pdf'] = url('uploads/pdf/' . $lecture->pdf);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Pdf Not Found';
        }
        return response()->json($response);
    }
    public function get_cource_pdf(Request $request)
    {
        $lectures = Cource::where('role', '=', 1)->get(['id', 'name', 'detail', 'pdf']);
        if ($lectures->count() > 0) {
            $response['status'] = true;

            $response['data'] = $lectures;
            foreach ($lectures as $lecture) {
                $lecture['pdf'] = url('uploads/cource/pdf/' . $lecture->pdf);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Cource Pdf Not Found';
        }
        return response()->json($response);
    }

    //this api work add views in video lecture with parameter lecture_id

    public function add_views(Request $request)
    {

        $lectures = Lecture::where('role', '=', 2)->where('id', $request->lecture_id)->first();
        if ($lectures) {
            $response['status'] = true;
            $response['data'] = $lectures;
            $lectures->view += 1;
            $lectures->save();
        } else {
            $response['status'] = true;
            $response['message'] = 'Lecture id Null';
        }
        return response()->json($response);

    }

    public function add_downloads_pdf(Request $request)
    {

        $lectures = Lecture::where('role', '=', 1)->where('id', $request->lecture_id)->first();
        if ($lectures) {
            $response['status'] = true;
            $response['data'] = $lectures;
            $lectures->download += 1;
            $lectures->save();
        } else {
            $response['status'] = true;
            $response['message'] = 'Lecture id Null';
        }
        return response()->json($response);

    }

    public function add_downloads_books(Request $request)
    {

        $lectures = Lecture::where('role', '=', 3)->where('id', $request->lecture_id)->first();
        if ($lectures) {
            $response['status'] = true;
            $response['data'] = $lectures;
            $lectures->download += 1;
            $lectures->save();
        } else {
            $response['status'] = true;
            $response['message'] = 'Lecture Id Null';
        }

        return response()->json($response);

    }

// thsis api work for get all events in events table from data base
    public function get_events(Request $request)
    {
        $events = Event::get();
        if ($events->count() > 0) {
            $response['status'] = true;

            $response['data'] = $events;
            foreach ($events as $event) {
                $event['gallery_1'] = url('assets/uploads/events/' . $event->gallery_1);
                $event['gallery_2'] = url('assets/uploads/events/' . $event->gallery_2);
                $event['gallery_3'] = url('assets/uploads/events/' . $event->gallery_3);
                $event['gallery_4'] = url('assets/uploads/events/' . $event->gallery_4);
                $event['gallery_5'] = url('assets/uploads/events/' . $event->gallery_5);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Gallery Not Found';
        }
        return response()->json($response);
    }

    public function get_up_events(Request $request)
    {
        $events = Event::where('date_of_event', '>=', Carbon::now())->get();
        if ($events->count() > 0) {

            $response['status'] = true;
            $response['data'] = $events;

            foreach ($events as $event) {
                $event['gallery_1'] = url('uploads/events/' . $event->gallery_1);
                $event['gallery_2'] = url('uploads/events/' . $event->gallery_2);
                $event['gallery_3'] = url('uploads/events/' . $event->gallery_3);
                $event['gallery_4'] = url('uploads/events/' . $event->gallery_4);
                $event['gallery_5'] = url('uploads/events/' . $event->gallery_5);
                $date = Carbon::parse($event->date_of_event);
                $now = Carbon::now();
                $event['remaining_days'] = $date->diffInDays($now);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Events Not Found';
        }
        return response()->json($response);
    }

    public function get_old_events(Request $request)
    {
        $events = Event::where('date_of_event', '<', Carbon::now())->get();
        if ($events->count() > 0) {

            $response['status'] = true;
            $response['data'] = $events;

            foreach ($events as $event) {
                $event['gallery_1'] = url('uploads/events/' . $event->gallery_1);
                $event['gallery_2'] = url('uploads/events/' . $event->gallery_2);
                $event['gallery_3'] = url('uploads/events/' . $event->gallery_3);
                $event['gallery_4'] = url('uploads/events/' . $event->gallery_4);
                $event['gallery_5'] = url('uploads/events/' . $event->gallery_5);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'Events Not Found';
        }
        return response()->json($response);
    }

    public function joind_events(Request $request)
    {

        $events = Event::where('id', $request->event_id)->where('date_of_event', '>=', Carbon::now())->first();
        if ($events) {
            $response['status'] = true;
            $response['data'] = $events;
            $events->interested_people += 1;
            $events->save();
        } else {
            $response['status'] = false;

            $response['message'] = 'Events Not Found Or Passed';
        }
        return response()->json($response);

    }

    public function add_joind_events(Request $request){
        $cutom_msg['phone_no.unique'] = 'this User already Joined this event';

        $validator = Validator::make($request->all(), [
          'name' => 'required|string|max:255',
          'event_id' => 'required|string|max:255',
          'phone_no' => 'required|string|max:255|unique:users',
       ],$cutom_msg);
       if ($validator->fails()){
           return response($validator->errors(),500);
       }

        $request->request->add([
            'name'=>$request->name,
            'phone_no'=>$request->phone_no,
            'role'=>3 ,
        ]);

        $create = User::create($request->all());


        if ($create) {

            $create->events()->syncWithoutDetaching($request->event_id);

            $response['status'] = true;
            $response['message'] = 'User join Successfully ';
        } else {
            $response['status'] = false;

            $response['message'] = 'user Not Join ';
        }
        return response()->json($response);

    }
    public function get_names(Request $request)
    {
        $scholars = Lecture::where('role', '=', 1)->get(['name']);
        if ($scholars->count() > 0) {
            $response['status'] = true;

            $response['data'] = $scholars;
//            foreach ($scholars as $scholar) {
//                $scholar['picture'] = url('/uploads/scholars/' . $scholar->picture);
//            }

        } else {
            $response['status'] = false;

            $response['message'] = 'not found';
        }
        return response()->json($response);
    }


    public function old_event(Request $request){
        //$now = Carbon::now()->toDateString();
        $events = Event::whereDate('date_of_event', '!=', Carbon::now()->toDateString())->get();

        if ($events->count() > 0) {
            $response['status'] = true;
            $response['data'] = $events;

            foreach ($events as $event) {
                $event['gallery_1'] = url('assets/uploads/events/' . $event->gallery_1);
                $event['gallery_2'] = url('assets/uploads/events/' . $event->gallery_2);
                $event['gallery_3'] = url('assets/uploads/events/' . $event->gallery_3);
                $event['gallery_4'] = url('assets/uploads/events/' . $event->gallery_4);
                $event['gallery_5'] = url('assets/uploads/events/' . $event->gallery_5);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'no Event';
        }
        return response()->json($response);
    }
    public function new_event(Request $request){
        //$now = Carbon::now()->toDateString();
        $events = Event::whereDate('date_of_event', '=', Carbon::now()->toDateString())->get();

        if ($events->count() > 0) {
            $response['status'] = true;
            $response['data'] = $events;

            foreach ($events as $event) {
                $event['gallery_1'] = url('assets/uploads/events/' . $event->gallery_1);
                $event['gallery_2'] = url('assets/uploads/events/' . $event->gallery_2);
                $event['gallery_3'] = url('assets/uploads/events/' . $event->gallery_3);
                $event['gallery_4'] = url('assets/uploads/events/' . $event->gallery_4);
                $event['gallery_5'] = url('assets/uploads/events/' . $event->gallery_5);
            }

        } else {
            $response['status'] = false;

            $response['message'] = 'no Event';
        }
        return response()->json($response);
    }


    public function ListenEvent(){

        return view('listenbroadcast');
    }

}
