<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = [

        'event_name',
        'date_of_event',
        'description',
        'interested_people',
        'date_of_event',
        'time_of_event',
        'seats_left',
        'total_seats',
        'gallery_1',
        'gallery_2',
        'gallery_3',
        'gallery_4',
        'gallery_5',
    ];

    public function users()
    {
        //return $this->belongsToMany(User::class);
        return $this->belongsToMany(User::class,'user_event','event_id','user_id');
       // return $this->belongsToMany(Event::class,'user_event','user_id','event_id');

    }
}
