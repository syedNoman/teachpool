<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
{

    Schema::create('education', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('user_id')->nullable();
        $table->string('education')->nullable();
        $table->string('uni_name')->nullable();
        $table->string('subject_studies')->nullable();
        $table->date('date_of_graduation')->nullable();
        $table->string('education_level')->nullable();
        $table->string('diploma_university_name')->nullable();
        $table->string('diploma_subject_studies')->nullable();
        $table->date('diploma_date_of_graduation')->nullable();

        $table->string('school_name1')->nullable();
        $table->string('school_subject1')->nullable();
        $table->string('school_grade1')->nullable();
        $table->date('school_date_achieved1')->nullable();

        $table->string('school_name2')->nullable();
        $table->string('school_subject2')->nullable();
        $table->string('school_grade2')->nullable();
        $table->date('school_date_achieved2')->nullable();

        $table->string('school_name3')->nullable();
        $table->string('school_subject3')->nullable();
        $table->string('school_grade3')->nullable();
        $table->date('school_date_achieved3')->nullable();

        $table->string('school_name4')->nullable();
        $table->string('school_subject4')->nullable();
        $table->string('school_grade4')->nullable();
        $table->date('school_date_achieved4')->nullable();

        $table->string('school_name5')->nullable();
        $table->string('school_subject5')->nullable();
        $table->string('school_grade5')->nullable();
        $table->date('school_date_achieved5')->nullable();

        $table->string('school_name6')->nullable();
        $table->string('school_subject6')->nullable();
        $table->string('school_grade6')->nullable();
        $table->date('school_date_achieved6')->nullable();

        $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
        $table->timestamps();
    });


}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
