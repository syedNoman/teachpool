<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
{

    Schema::create('referees', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('user_id')->nullable();
        $table->string('reference_name')->nullable();
        $table->string('reference_occupation')->nullable();
        $table->text('reference_address')->nullable();
        $table->string('reference_email')->nullable();
        $table->string('name_capital')->nullable();
        $table->date('date')->nullable();

        $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
        $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
