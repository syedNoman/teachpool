<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
{

    Schema::create('employment_detail', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('user_id')->nullable();
        $table->integer('employed')->nullable();
        $table->string('company_name')->nullable();
        $table->string('job_title')->nullable();
        $table->string('department')->nullable();
        $table->text('company_address')->nullable();

        $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade')->nullable();
        $table->timestamps();
    });


}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
