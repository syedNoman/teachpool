<?php

use Illuminate\Database\Seeder;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'admin',
            'middle_name' => 'admin',
            'family_name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin123456'),
            'role' => 1,
        ]);
    }
}
